Feature: Create Task
	As a ToDo App user
	I should be able to create a task
	So I can manage my tasks
	
	
	#Acceptance Criteria
	@Runme
	Scenario: Login
		Given that the user has the page open
		When the user inserts his credentials
		Then he press the login button
	
	@Runme
	Scenario: Check Navigation Bar
		Given that the user is logged in
		When the main page appears
		Then he should see My Tasks on the Navigation Bar
	
	@Runme
	Scenario: Open Task Lists
		Given that the user is on the main page
		When the user clicks on the My Tasks link
		Then the task list should be opened
	
	@Runme
	Scenario: Check logged in users list
		Given that the user has the list open
		Then the list should belong to him
		
	@Runme
	Scenario: Create Task with + button
		Given that the user is on the task page
		When he enters the task name
		And press the plus button
		Then this task should be created
	
	@Runme
	Scenario: Create task with enter key
		Given that the user is on the task page
		When he enters the task name
		And hits the return key
		Then this task should be created
		
	@Runme
	Scenario: Validate short task name
		Given that the user has entered a task with less than three chars
		When he hits the enter button
		Then his task should not be created
	
	@Runme
	Scenario: Validate long task name
		Given that the user has entender a task with more than 250 chars
		Then the task title should be cut
	
	@Runme
	Scenario: Check created task
		Given that the user has entered his task
		Then his task should appear on the table
