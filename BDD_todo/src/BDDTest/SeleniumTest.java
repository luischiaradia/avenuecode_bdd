package BDDTest;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.*;
import org.testng.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;


public class SeleniumTest {
	private WebDriver driver;
	private String baseUrl;
	private String browserName;
	private String browserVersion;

	public void setUp() throws Exception {
		//driver = new ChromeDriver();
		//System.setProperty("webdriver.chrome.driver", "/Users/Luis/Downloads/chromedriver");
		baseUrl = "http://qa-test.avenuecode.com/";
		//driver = new HtmlUnitDriver();
		driver = new FirefoxDriver();
		//driver = new  InternetExplorerDriver ();
		//driver = new  SafariDriver();
	}
	
	public void tearDown() {
		driver.quit();
	}
	
	public void goToHomePage() {
		driver.get(baseUrl);
	}
	
	public void insertUserCredentials (String email, String password) throws Exception {
		//This checks if the 'Sign in' link appears on the top right.
		boolean userIsNotLoggedIn = driver.findElement(By.linkText("Sign In")).isDisplayed();
		//If so, then log in.
		if (userIsNotLoggedIn) {
			driver.findElement(By.linkText("Sign In")).click(); //Clicks on the link.
			driver.findElement(By.id("user_email")).sendKeys(email); //Inserts the email
			driver.findElement(By.id("user_password")).sendKeys(password);//inserts the password
		}
	}
	
	public void pressLogin (String taskName) {
		driver.findElement(By.name("commit")).click(); //Click to sign in.

	}
	
	public void checkTasks() {
		assertTrue(driver.findElement(By.linkText("My Tasks")).isDisplayed());		
	}
	
	public void openMyTasks () {
		driver.findElement(By.linkText("My Tasks")).click(); //If so, click on it.
	}

	
	public void createNewTaskWithEnterKey (String taskName) {
		//Inserts the task name.
		driver.findElement(By.id("new_task")).sendKeys(taskName);
		//Press ENTER key
		WebElement we = driver.findElement(By.id("new_task"));
		we.sendKeys(Keys.ENTER);
	}
	
	public void createNewTaskWithPlusButton (String taskName) {
		driver.findElement(By.id("new_task")).sendKeys(taskName);
		
		//Clicks on the "+" button.
		driver.findElement(By.xpath("//div[2]/span")).click();

	}
	
	public void validateCreatedTask () {
		String taskInsertedSize = driver.findElement(By.id("new_task")).getAttribute("value");
		int stringLen = taskInsertedSize.length();
		assertEquals(stringLen, 0);
	}
	
	public void insertShortTaskName () {
		String shortTaskName = "a";
		driver.findElement(By.id("new_task")).sendKeys(shortTaskName);
	}
	
	public void createShortTask () {
		//Press ENTER
		WebElement we = driver.findElement(By.id("new_task"));
		we.sendKeys(Keys.ENTER);
	}
	
	public void validateShortTask() {
		//Gets what is inserted to see if it empty.
		String taskInsertedSize = driver.findElement(By.id("new_task")).getAttribute("value");
		int stringLen = taskInsertedSize.length();
		assertNotEquals(0, stringLen);
	}
	
	public void checkLongTask () {
		//Gets the string from the field e see if it has more than 250 characters
		String taskInsertedSize = driver.findElement(By.id("new_task")).getAttribute("value");
		int stringLen = taskInsertedSize.length();
		//Cleans the field
		driver.findElement(By.id("new_task")).sendKeys("");
		assertEquals(250, stringLen);
	}
	
	public void insertLongTaskName () {
		//This string has 300 characters.
		//The next block of code does the following:
		//Inserts the task name.
		String longTaskName = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec.";
		driver.findElement(By.id("new_task")).sendKeys(longTaskName);
	}

	public void checkTaskListOwner() {
		String taskHeaderTitle = driver.findElement(By.tagName("H1")).getText();
		String[] titleArray = taskHeaderTitle.split("'");
		String userName = titleArray[0];
		
		assertEquals(taskHeaderTitle, userName);
	}
	
	public void checkCreatedTask() {
		//Gets the object
		WebElement table = driver.findElement(By.className("table"));
		//Gets the elements to see if it was created.
		String elements = table.getAttribute("innerText");
		//Fiquei na duvida de como comparar com uma parte da String.
		//assert
	}
	
	public void checkSubtaskButton () {
		assertTrue(driver.findElement(By.xpath("//td[4]/button")).isDisplayed());
	}
	
	public void checkNumberOfSubtasks () {
		//Nao encontrei um metodo de comparar isso.
	}
	
	public void openSubtaskWindow() {
		driver.findElement(By.xpath("//td[4]/button")).click();
	}
	
	public void checkReadOnlyFields () {
		assertEquals(driver.findElement(By.tagName("H3")).getAttribute("isContentEditable"), "false");
		assertEquals(driver.findElement(By.id("edit_task")).getAttribute("readOnly"), "true");
	}
	
	public void validateSubtaskLength() {
		//This string has 300 characters.
		String longTaskName = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec.";
		//The next block of code does the following:
		//Inserts the task name.
		//Gets the string from the field e see if it has more than 250 characters
		driver.findElement(By.id("new_sub_task")).sendKeys(longTaskName);
		String taskInsertedSize = driver.findElement(By.id("new_sub_task")).getAttribute("value");
		int stringLen = taskInsertedSize.length();
		driver.findElement(By.id("new_sub_task")).sendKeys("");
		assertEquals(250, stringLen);
	}
	public void validateSubTaskDueDate() {
		//Formats the date according requirements.
		SimpleDateFormat sdfDate = new SimpleDateFormat("MM/DD/YYYY");
		Date now = new Date();
		String dueDate = sdfDate.format(now);
		//Inserts it.
		driver.findElement(By.id("dueDate")).sendKeys(dueDate);
	}
	
	public void checkRequiredFields() {
		//Checks if the subtask filed is required.
		assertEquals(driver.findElement(By.id("new_sub_task")).getAttribute("required"), "true");
		//Checks if the dueDate field is required.
		assertEquals(driver.findElement(By.id("dueDate")).getAttribute("required"), "true");
	}
	
	public void addSubTask () {
		driver.findElement(By.id("add-subtask")).click();
	}
	
	public void validateSubtask (){
		//Nao encontrei uma maneira de comparar as tasks criadas
	}

	
	
}
