package BDDTest;

import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.en.*;

public class StepDefs {
	private SeleniumTest script = new SeleniumTest();
	public void setUpWebDriver() throws Exception {
	   
	   script.setUp();
	   script.goToHomePage();
	}

	// This block of code is for the "Login" scenario
	@Given("^that the user has the page open$")
	public void that_the_user_has_the_page_open() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		setUpWebDriver();
	    //throw new PendingException();
	}

	@When("^the user inserts his credentials$")
	public void the_user_inserts_his_credentials() throws Throwable {
		script.insertUserCredentials("luisfelipe.sbs@gmail.com", "avenueluis");
	}

	@Then("^he press the login button$")
	public void he_press_the_login_button() throws Throwable {
		script.pressLogin("Task");
	}
	//End of Login
	
	@Given("^that the user is logged in$")
	public void that_the_user_is_logged_in() throws Throwable {
//	    // Write code here that turns the phrase above into concrete actions
//	    throw new PendingException();
	}

	@When("^the main page appears$")
	public void the_main_page_appears() throws Throwable {
//	    // Write code here that turns the phrase above into concrete actions
//	    throw new PendingException();
	}

	@Then("^he should see My Tasks on the Navigation Bar$")
	public void he_should_see_My_Tasks_on_the_Navigation_Bar() throws Throwable {
		script.checkTasks(); //NullPointerException
//	    // Write code here that turns the phrase above into concrete actions
//	    throw new PendingException();
	}
	
	@Given("^that the user is on the main page$")
	public void that_the_user_is_on_the_main_page() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

	@When("^the user clicks on the My Tasks link$")
	public void the_user_clicks_on_the_My_Tasks_link() throws Throwable {
		script.openMyTasks();
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

	@Then("^the task list should be opened$")
	public void the_task_list_should_be_opened() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}
	
	@Given("^that the user has the list open$")
	public void that_the_user_has_the_list_open() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

	@Then("^the list should belong to him$")
	public void the_list_should_belong_to_him() throws Throwable {
		script.checkTaskListOwner();
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}
	
	@Given("^that the user is on the task page$")
	public void that_the_user_is_on_the_task_page() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

	@When("^he enters the task name$")
	public void he_enters_the_task_name() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   // throw new PendingException();
	}

	@When("^press the plus button$")
	public void press_the_plus_button() throws Throwable {
		script.createNewTaskWithPlusButton("Testing task for + button");
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

	@Then("^this task should be created$")
	public void this_task_should_be_created() throws Throwable {
		script.validateCreatedTask();
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

	@When("^hits the return key$")
	public void hits_the_return_key() throws Throwable {
		script.createNewTaskWithEnterKey("Testing task for ENTER key");
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

	@Given("^that the user has entered a task with less than three chars$")
	public void that_the_user_has_entered_a_task_with_less_than_three_chars() throws Throwable {
	    script.insertShortTaskName ();
		// Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

	@When("^he hits the enter button$")
	public void he_hits_the_enter_button() throws Throwable {
		script.createShortTask ();
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

	@Then("^his task should not be created$")
	public void his_task_should_not_be_created() throws Throwable {
		script.validateShortTask();
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

	@Given("^that the user has entender a task with more than (\\d+) chars$")
	public void that_the_user_has_entender_a_task_with_more_than_chars(int arg1) throws Throwable {
		script.insertLongTaskName ();
		// Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}
	
	@Then("^the task title should be cut$")
	public void the_task_title_should_be_cut() {
		script.checkLongTask();
	}

	@Given("^that the user has entered his task$")
	public void that_the_user_has_entered_his_task() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

//	@When("^he hits the enter key$")
//	public void he_hits_the_enter_key() throws Throwable {
//	    // Write code here that turns the phrase above into concrete actions
//	    //throw new PendingException();
//	}

	@Then("^his task should appear on the table$")
	public void his_task_should_appear_on_the_table() throws Throwable {
		
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}
	
	@Given("^that the user has tasks$")
	public void that_the_user_has_tasks() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

	@Then("^he should see a Manage Subtasks button on them$")
	public void he_should_see_a_Manage_Subtasks_button_on_them() throws Throwable {
		script.checkSubtaskButton();
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

	@Given("^that the subtask button appears$")
	public void that_the_subtask_button_appears() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

	@Then("^should appear the number of subtasks on it$")
	public void should_appear_the_number_of_subtasks_on_it() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

	@Given("^that the user clicks on the Manage Subtask bt$")
	public void that_the_user_clicks_on_the_Manage_Subtask_bt() throws Throwable {
		script.openSubtaskWindow();
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

	@Given("^the TaskID and Task Description are readonly$")
	public void the_TaskID_and_Task_Description_are_readonly() throws Throwable {
		script.checkReadOnlyFields();
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

	@Given("^the subtask description does not allow more than '(\\d+)' chars with the due date field$")
	public void the_subtask_description_does_not_allow_more_than_chars_with_the_due_date_field(int arg1) throws Throwable {
	    script.validateSubtaskLength();
	    script.validateSubTaskDueDate();
		// Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

	@Given("^the Task Description and Due Date are required fields$")
	public void the_Task_Description_and_Due_Date_are_required_fields() throws Throwable {
		script.checkRequiredFields();
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

	@When("^he clicks to add$")
	public void he_clicks_to_add() throws Throwable {
		script.addSubTask();
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

	@Then("^the subtask should appear on the table$")
	public void the_subtask_should_appear_on_the_table() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}
	
	@After
	public void tidyUp() {
		script.tearDown();
	}
}
