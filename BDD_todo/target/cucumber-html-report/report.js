$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("subtasks.feature");
formatter.feature({
  "line": 1,
  "name": "Create SubTask",
  "description": "As a ToDo App user\nI should be able to create a subtask\nSo I can break down my tasks in smaller pieces",
  "id": "create-subtask",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 7,
  "name": "See Manage Subtask bt",
  "description": "",
  "id": "create-subtask;see-manage-subtask-bt",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 6,
      "name": "@Runme"
    }
  ]
});
formatter.step({
  "line": 8,
  "name": "that the user has tasks",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "he should see a Manage Subtasks button on them",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "line": 12,
  "name": "Check number of subtasks",
  "description": "",
  "id": "create-subtask;check-number-of-subtasks",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 11,
      "name": "@Runme"
    }
  ]
});
formatter.step({
  "line": 13,
  "name": "that the subtask button appears",
  "keyword": "Given "
});
formatter.step({
  "line": 14,
  "name": "should appear the number of subtasks on it",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "line": 16,
  "name": "Create subtask",
  "description": "",
  "id": "create-subtask;create-subtask",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 15,
      "name": "@Runme"
    }
  ]
});
formatter.step({
  "line": 17,
  "name": "that the user clicks on the Manage Subtask bt",
  "keyword": "Given "
});
formatter.step({
  "line": 18,
  "name": "the TaskID and Task Description are readonly",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "the subtask description does not allow more than \u0027250\u0027 chars with the due date field",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "the Task Description and Due Date are required fields",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "he clicks to add",
  "keyword": "When "
});
formatter.step({
  "line": 22,
  "name": "the subtask should appear on the table",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.uri("todo_app.feature");
formatter.feature({
  "line": 1,
  "name": "Create Task",
  "description": "As a ToDo App user\r\nI should be able to create a task\r\nSo I can manage my tasks",
  "id": "create-task",
  "keyword": "Feature"
});
formatter.scenario({
  "comments": [
    {
      "line": 7,
      "value": "#Acceptance Criteria"
    }
  ],
  "line": 9,
  "name": "Login",
  "description": "",
  "id": "create-task;login",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 8,
      "name": "@Runme"
    }
  ]
});
formatter.step({
  "line": 10,
  "name": "that the user has the page open",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "the user inserts his credentials",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "he press the login button",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "line": 15,
  "name": "Check Navigation Bar",
  "description": "",
  "id": "create-task;check-navigation-bar",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 14,
      "name": "@Runme"
    }
  ]
});
formatter.step({
  "line": 16,
  "name": "that the user is logged in",
  "keyword": "Given "
});
formatter.step({
  "line": 17,
  "name": "the main page appears",
  "keyword": "When "
});
formatter.step({
  "line": 18,
  "name": "he should see My Tasks on the Navigation Bar",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "line": 21,
  "name": "Open Task Lists",
  "description": "",
  "id": "create-task;open-task-lists",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 20,
      "name": "@Runme"
    }
  ]
});
formatter.step({
  "line": 22,
  "name": "that the user is on the main page",
  "keyword": "Given "
});
formatter.step({
  "line": 23,
  "name": "the user clicks on the My Tasks link",
  "keyword": "When "
});
formatter.step({
  "line": 24,
  "name": "the task list should be opened",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "line": 27,
  "name": "Check logged in users list",
  "description": "",
  "id": "create-task;check-logged-in-users-list",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 26,
      "name": "@Runme"
    }
  ]
});
formatter.step({
  "line": 28,
  "name": "that the user has the list open",
  "keyword": "Given "
});
formatter.step({
  "line": 29,
  "name": "the list should belong to him",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "line": 32,
  "name": "Create Task with + button",
  "description": "",
  "id": "create-task;create-task-with-+-button",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 31,
      "name": "@Runme"
    }
  ]
});
formatter.step({
  "line": 33,
  "name": "that the user is on the task page",
  "keyword": "Given "
});
formatter.step({
  "line": 34,
  "name": "he enters the task name",
  "keyword": "When "
});
formatter.step({
  "line": 35,
  "name": "press the plus button",
  "keyword": "And "
});
formatter.step({
  "line": 36,
  "name": "this task should be created",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "line": 39,
  "name": "Create task with enter key",
  "description": "",
  "id": "create-task;create-task-with-enter-key",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 38,
      "name": "@Runme"
    }
  ]
});
formatter.step({
  "line": 40,
  "name": "that the user is on the task page",
  "keyword": "Given "
});
formatter.step({
  "line": 41,
  "name": "he enters the task name",
  "keyword": "When "
});
formatter.step({
  "line": 42,
  "name": "hits the return key",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "this task should be created",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "line": 46,
  "name": "Validate short task name",
  "description": "",
  "id": "create-task;validate-short-task-name",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 45,
      "name": "@Runme"
    }
  ]
});
formatter.step({
  "line": 47,
  "name": "that the user has entered a task with less than three chars",
  "keyword": "Given "
});
formatter.step({
  "line": 48,
  "name": "he hits the enter button",
  "keyword": "When "
});
formatter.step({
  "line": 49,
  "name": "his task should not be created",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "line": 52,
  "name": "Validate long task name",
  "description": "",
  "id": "create-task;validate-long-task-name",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 51,
      "name": "@Runme"
    }
  ]
});
formatter.step({
  "line": 53,
  "name": "that the user has entender a task with more than 250 chars",
  "keyword": "Given "
});
formatter.step({
  "line": 54,
  "name": "the task title should be cut",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "line": 57,
  "name": "Check created task",
  "description": "",
  "id": "create-task;check-created-task",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 56,
      "name": "@Runme"
    }
  ]
});
formatter.step({
  "line": 58,
  "name": "that the user has entered his task",
  "keyword": "Given "
});
formatter.step({
  "line": 59,
  "name": "his task should appear on the table",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
});