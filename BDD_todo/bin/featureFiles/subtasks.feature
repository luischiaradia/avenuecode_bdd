Feature: Create SubTask
	As a ToDo App user
	I should be able to create a subtask
	So I can break down my tasks in smaller pieces
	
	@Runme
	Scenario: See Manage Subtask bt
		Given that the user has tasks
		Then he should see a Manage Subtasks button on them
	
	@Runme
	Scenario: Check number of subtasks
		Given that the subtask button appears
		Then should appear the number of subtasks on it
	@Runme
	Scenario: Create subtask
		Given that the user clicks on the Manage Subtask bt
		And the TaskID and Task Description are readonly
		And the subtask description does not allow more than '250' chars with the due date field
		And the Task Description and Due Date are required fields
		When he clicks to add
		Then the subtask should appear on the table